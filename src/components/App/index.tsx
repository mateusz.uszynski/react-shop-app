import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Container, makeStyles } from '@material-ui/core';
import { TopBar, Footer } from 'components';
import { ProductPage, CartPage, AddressPage, ProductDetailPage } from 'pages';
import { AuthContextProvider, CartContextProvider } from 'services';
import { SnackbarProvider } from 'notistack';

const useStyles = makeStyles((theme) => ({
    main: {
        flexGrow: 1,
        flexShrink: 0,
        marginTop: '60px',
    },
}));

export function App() {
    const classes = useStyles();

    return (
        <BrowserRouter>
            <AuthContextProvider>
                <CartContextProvider>
                    <SnackbarProvider
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        autoHideDuration={1000}
                    >
                        <>
                            <TopBar />
                            <Container maxWidth="lg" component="main" className={classes.main}>
                                <Switch>
                                    <Route path="/" exact>
                                        <ProductPage />
                                    </Route>
                                    <Route path="/products" component={ProductPage} />
                                    <Route path="/product/:id" component={ProductDetailPage} />
                                    <Route path="/cart" component={CartPage} />
                                    <Route path="/address" component={AddressPage} />
                                </Switch>
                            </Container>
                            <Footer />
                        </>
                    </SnackbarProvider>
                </CartContextProvider>
            </AuthContextProvider>
        </BrowserRouter>
    );
}
