import React from 'react';
import { render } from '@testing-library/react';
import { App } from '.';

test('renders login link', () => {
    const { getByText } = render(<App />);
    const linkElement = getByText(/Log in/i);
    expect(linkElement).toBeInTheDocument();
});
