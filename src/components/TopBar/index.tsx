import React, { useCallback, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext, firebaseService, CartContext } from 'services';
import { UserMenu, LoginButton, CartButton, Logo } from './components';
import { Toolbar, AppBar } from '@material-ui/core';
import { useSnackbar } from 'notistack';

export function TopBar() {
    const { user } = useContext(AuthContext);
    const { cart } = useContext(CartContext);
    const history = useHistory();
    const { enqueueSnackbar } = useSnackbar();

    const handleLogoClick = useCallback(() => {
        history.push('/');
    }, [history]);

    const handleCartClick = useCallback(() => {
        history.push('/cart');
    }, [history]);

    const handleLoginButtonClick = useCallback(() => {
        firebaseService
            .signInWithGoogle()
            .then(() => enqueueSnackbar('Login succeeded', { variant: 'success', autoHideDuration: 2000 }))
            .catch(() => enqueueSnackbar('Something went wrong', { variant: 'error', autoHideDuration: 2000 }));
    }, [enqueueSnackbar]);

    const handleSignOut = useCallback(() => {
        firebaseService
            .signOut()
            .then(() => enqueueSnackbar('Logout succeeded', { variant: 'success', autoHideDuration: 2000 }))
            .catch(() => enqueueSnackbar('Something went wrong', { variant: 'error', autoHideDuration: 2000 }));
    }, [enqueueSnackbar]);

    return (
        <div>
            <AppBar position="fixed">
                <Toolbar>
                    <Logo logoClick={handleLogoClick} />
                    <CartButton itemsCount={cart.length} cartClick={handleCartClick} />
                    {user === null && <LoginButton login={handleLoginButtonClick} />}
                    {user !== null && <UserMenu signOut={handleSignOut} />}
                </Toolbar>
            </AppBar>
        </div>
    );
}
