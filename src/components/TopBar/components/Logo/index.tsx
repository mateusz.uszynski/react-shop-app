import React from 'react';
import { Typography, makeStyles, Theme, createStyles } from '@material-ui/core';
import { Storefront as StorefrontIcon } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        title: {
            flexGrow: 1,
            display: 'flex',
            alignItems: 'center',
            cursor: 'pointer',
        },
        logoIcon: {
            fontSize: '2rem',
            marginRight: theme.spacing(1),
        },
    }),
);

interface LogoProps {
    readonly logoClick: () => void;
}

export function Logo({ logoClick }: LogoProps) {
    const classes = useStyles();

    return (
        <Typography variant="h6" className={classes.title} onClick={logoClick}>
            <StorefrontIcon className={classes.logoIcon} /> Shop App
        </Typography>
    );
}
