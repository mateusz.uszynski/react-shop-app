import React, { useState, useCallback, useRef } from 'react';
import { IconButton, Menu, MenuItem, PopoverOrigin } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';

const anchorOrigin: PopoverOrigin = {
    vertical: 'top',
    horizontal: 'right',
};

const transformOrigin: PopoverOrigin = {
    vertical: 'top',
    horizontal: 'right',
};

interface UserMenuProps {
    readonly signOut: () => void;
}

export function UserMenu({ signOut }: UserMenuProps) {
    const iconRef = useRef<HTMLButtonElement>(null);
    const [isOpen, setIsOpen] = useState(false);

    const handleMenu = useCallback(() => {
        setIsOpen(true);
    }, []);

    const handleClose = useCallback(() => {
        setIsOpen(false);
    }, []);

    return (
        <div>
            <IconButton
                aria-label="current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                onClick={handleMenu}
                ref={iconRef}
            >
                <AccountCircle />
            </IconButton>
            <Menu
                id="user-menu"
                anchorEl={iconRef.current}
                anchorOrigin={anchorOrigin}
                keepMounted
                transformOrigin={transformOrigin}
                open={isOpen}
                onClose={handleClose}
            >
                <MenuItem onClick={signOut}>Sign out</MenuItem>
            </Menu>
        </div>
    );
}
