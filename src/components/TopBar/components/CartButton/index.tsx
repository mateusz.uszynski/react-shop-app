import React from 'react';
import { IconButton, Badge, makeStyles, Theme, createStyles } from '@material-ui/core';
import { ShoppingCart as ShoppingCartIcon } from '@material-ui/icons';

interface CartButtonProps {
    readonly itemsCount: number;
    readonly cartClick: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        cartBadge: {
            marginRight: theme.spacing(3),
        },
    }),
);

export function CartButton({ itemsCount, cartClick }: CartButtonProps) {
    const classes = useStyles();

    return (
        <IconButton aria-label="cart" className={classes.cartBadge} color="inherit" onClick={cartClick}>
            <Badge badgeContent={itemsCount} color="secondary">
                <ShoppingCartIcon />
            </Badge>
        </IconButton>
    );
}
