import React, { useEffect, useState, useCallback, useContext, MouseEvent } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Product, ProductId } from 'types';
import { getProduct, CartContext, CartActionType, CartAction } from 'services';
import {
    CircularProgress,
    Paper,
    Grid,
    Avatar,
    makeStyles,
    Theme,
    createStyles,
    Button,
    Chip,
} from '@material-ui/core';
import {
    Help as HelpIcon,
    AddShoppingCart as AddShoppingCartIcon,
    ArrowBack as ArrowBackIcon,
    AttachMoney as AttachMoneyIcon,
} from '@material-ui/icons';
import { useSnackbar } from 'notistack';
import { isInCart } from 'utils';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        name: {
            margin: '0',
            fontSize: '1.2rem',
            fontWeight: 'bold',
        },
        avatar: {
            maxWidth: theme.spacing(30),
            maxHeight: theme.spacing(30),
            width: '100%',
            height: 'auto',
        },
        description: {
            textAlign: 'justify',
            margin: '20px 20px',
        },
        button: {
            margin: '2px 5px',
        },
    }),
);

export function ProductDetailPage() {
    const { id } = useParams();
    const { cart, dispatchCart } = useContext(CartContext);
    const { enqueueSnackbar } = useSnackbar();
    const [product, setProduct] = useState<Product | null>(null);
    const styles = useStyles();
    const history = useHistory();

    useEffect(() => {
        setTimeout(() => {
            getProduct(id).then(setProduct);
        }, 500);
    }, [id]);

    const handleGoBack = useCallback(() => {
        history.goBack();
    }, [history]);

    const handleAddToCart = useCallback(
        (e: MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            dispatchCart({ type: CartActionType.ADD_TO_CART, productId: id as ProductId } as CartAction);
            enqueueSnackbar('The product has been added to the cart', { variant: 'success' });
        },
        [id, dispatchCart, enqueueSnackbar],
    );

    return (
        <div style={{ padding: 16, margin: 'auto', maxWidth: 600 }}>
            <h1>Product details</h1>
            {product === null && <CircularProgress />}
            {product !== null && (
                <>
                    <Grid container alignItems="center" justify="space-between" spacing={2}>
                        <Button
                            className={styles.button}
                            onClick={handleGoBack}
                            variant="contained"
                            color="primary"
                            startIcon={<ArrowBackIcon />}
                        >
                            Go back
                        </Button>
                        <Button
                            className={styles.button}
                            onClick={handleAddToCart}
                            variant="contained"
                            color="primary"
                            startIcon={<AddShoppingCartIcon />}
                            disabled={isInCart(product.id, cart)}
                        >
                            Add to cart
                        </Button>
                    </Grid>
                    <Paper style={{ padding: 20, marginTop: 30 }}>
                        <Grid container justify="center" spacing={2}>
                            <Grid justify="space-between" item container>
                                <Grid item>
                                    <p className={styles.name}>{product.name}</p>
                                </Grid>
                                <Grid item>
                                    <Chip
                                        variant="outlined"
                                        color="primary"
                                        label={product.price}
                                        icon={<AttachMoneyIcon />}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Avatar
                                    alt={product.name}
                                    src={product.image}
                                    variant="rounded"
                                    className={styles.avatar}
                                >
                                    <HelpIcon className={styles.avatar} />
                                </Avatar>
                            </Grid>
                            <Grid item className={styles.description}>
                                {product.description}
                            </Grid>
                        </Grid>
                    </Paper>
                </>
            )}
        </div>
    );
}
