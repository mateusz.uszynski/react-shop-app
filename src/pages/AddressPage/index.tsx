import React, { useCallback, useContext, useEffect } from 'react';
import { Form } from 'react-final-form';
import { Paper, Grid, Button, MenuItem, Typography } from '@material-ui/core';
import { Send as SendIcon } from '@material-ui/icons';
import { Select, TextField } from 'mui-rff';
import { AddressForm, Field } from 'types';
import { validateAddressForm, CartContext, AuthContext, CartAction, CartActionType } from 'services';
import { useSnackbar } from 'notistack';
import { useHistory } from 'react-router-dom';
import { addOrder } from 'services/orders';

const formFields: Array<Field> = [
    {
        size: 12,
        field: (
            <Typography variant="h6" color="primary" style={{ marginTop: '10px' }}>
                Contact Name
            </Typography>
        ),
    },
    {
        size: 2,
        field: (
            <Select name="title" label="Title" formControlProps={{ margin: 'none' }}>
                <MenuItem value=""></MenuItem>
                <MenuItem value="mr">Mr</MenuItem>
                <MenuItem value="ms">Ms</MenuItem>
            </Select>
        ),
    },
    {
        size: 5,
        field: <TextField label="First name" name="firstName" margin="none" required={true} />,
    },
    {
        size: 5,
        field: <TextField label="Last name" name="lastName" margin="none" required={true} />,
    },
    {
        size: 12,
        field: (
            <Typography variant="h6" color="primary" style={{ marginTop: '20px' }}>
                Address
            </Typography>
        ),
    },
    {
        size: 12,
        field: <TextField label="Street" name="street" margin="none" required={true} />,
    },
    {
        size: 6,
        field: <TextField label="Building number" name="buildingNo" margin="none" required={true} />,
    },
    {
        size: 6,
        field: <TextField label="Apartment number" name="apartmentNo" margin="none" />,
    },
    {
        size: 4,
        field: <TextField label="Postal Code" name="postalCode" margin="none" required={true} />,
    },
    {
        size: 8,
        field: <TextField label="City" name="city" margin="none" required={true} />,
    },
    {
        size: 12,
        field: <TextField label="Country" name="country" margin="none" required={true} />,
    },
];

export function AddressPage() {
    const { user } = useContext(AuthContext);
    const { cart, dispatchCart } = useContext(CartContext);
    const history = useHistory();
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        if (cart.length < 1) history.push('/');
    }, [cart, history]);

    const handleSubmit = useCallback(
        (data: AddressForm) => {
            if (!user) enqueueSnackbar('You have to log in first', { variant: 'warning', autoHideDuration: 3000 });
            else {
                addOrder(data, cart, user);
                dispatchCart({ type: CartActionType.CLEAR_CART } as CartAction);
                enqueueSnackbar('Your order has been submitted', { variant: 'success', autoHideDuration: 3000 });
            }
        },
        [cart, user, enqueueSnackbar, dispatchCart],
    );

    return (
        <div style={{ padding: 16, margin: 'auto', maxWidth: 600 }}>
            <h2>Shipping Form</h2>
            <Form
                onSubmit={handleSubmit}
                validate={validateAddressForm}
                render={({ handleSubmit, submitting }) => (
                    <form onSubmit={handleSubmit} noValidate>
                        <Paper style={{ padding: 16 }}>
                            <Grid container alignItems="flex-start" spacing={2}>
                                {formFields.map((field: Field, id: number) => (
                                    <Grid item xs={field.size} key={id}>
                                        {field.field}
                                    </Grid>
                                ))}
                            </Grid>
                            <Grid container justify="center" spacing={2}>
                                <Grid item style={{ marginTop: 16 }}>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        disabled={submitting}
                                        endIcon={<SendIcon />}
                                    >
                                        Submit
                                    </Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </form>
                )}
            />
        </div>
    );
}
