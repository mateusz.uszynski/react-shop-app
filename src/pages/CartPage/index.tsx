import React, { useCallback, MouseEvent, useEffect, useState, useContext } from 'react';
import { CartActionType } from 'services';
import { Paper, List, Theme, makeStyles, createStyles, Typography, Button } from '@material-ui/core';
import { Send as SendIcon } from '@material-ui/icons';
import { getAllProducts, CartContext, CartAction } from 'services';
import { find } from 'lodash';
import { CartItem } from './components';
import { Product, ProductId } from 'types';
import { getTotalPrice } from 'utils';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        price: {
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
            margin: '20px 30px',
        },
        button: {},
    }),
);

export function CartPage() {
    const { cart, dispatchCart } = useContext(CartContext);
    const [products, setProducts] = useState<Product[]>([]);
    const history = useHistory();
    const styles = useStyles();

    useEffect(() => {
        getAllProducts().then((resp) => setProducts(resp.data));
    }, []);

    const fireAction = useCallback(
        (productId: ProductId, type: CartActionType) => (e: MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            dispatchCart({ type, productId } as CartAction);
        },
        [dispatchCart],
    );

    const handleGoNext = useCallback(() => {
        history.push('/address');
    }, [history]);

    return (
        <div>
            <h2>Your cart</h2>
            {cart.length === 0 && <p>Cart is empty!</p>}
            {cart.length > 0 && (
                <>
                    <Paper>
                        <List>
                            {cart.map(({ id, count }) => (
                                <CartItem
                                    id={id}
                                    key={id}
                                    count={count}
                                    product={find(products, { id })}
                                    fireAction={fireAction}
                                />
                            ))}
                            <div className={styles.price}>
                                <Typography variant="h6" style={{ marginRight: '10px' }}>
                                    Total price:
                                </Typography>
                                <Typography variant="h5" color="primary">
                                    {getTotalPrice(products, cart)}$
                                </Typography>
                            </div>
                        </List>
                    </Paper>
                    <div className={styles.price}>
                        <Button variant="contained" color="primary" endIcon={<SendIcon />} onClick={handleGoNext}>
                            Go next
                        </Button>
                    </div>
                </>
            )}
        </div>
    );
}
