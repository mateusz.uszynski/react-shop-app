import React, { MouseEvent } from 'react';
import { ListItem, ListItemText, IconButton, ListItemSecondaryAction } from '@material-ui/core';
import { Add as AddIcon, Remove as RemoveIcon, Delete as DeleteIcon } from '@material-ui/icons';
import { CartActionType } from 'services';
import { ProductId, Product } from 'types';

interface CartItemProps {
    id: ProductId;
    count: number;
    product: Product | undefined;
    fireAction: (productId: ProductId, type: CartActionType) => (e: MouseEvent<HTMLButtonElement>) => void;
}

export function CartItem({ id, product, count, fireAction }: CartItemProps) {
    return (
        <ListItem>
            <ListItemText primary={product?.name} secondary={`Count: ${count}`} />
            <ListItemSecondaryAction>
                <IconButton onClick={fireAction(id, CartActionType.INCREMENT)}>
                    <AddIcon />
                </IconButton>
                <IconButton onClick={fireAction(id, CartActionType.DECREMENT)}>
                    <RemoveIcon />
                </IconButton>
                <IconButton onClick={fireAction(id, CartActionType.REMOVE_FROM_CART)}>
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    );
}
