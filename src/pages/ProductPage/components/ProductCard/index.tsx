import React, { useCallback, MouseEvent } from 'react';
import { useHistory } from 'react-router-dom';
import { Card, Avatar, makeStyles, createStyles, Theme, Grid, Chip, Button } from '@material-ui/core';
import {
    AddShoppingCart as AddShoppingCartIcon,
    ArrowForward as ArrowForwardIcon,
    AttachMoney as AttachMoneyIcon,
    Help as HelpIcon,
} from '@material-ui/icons';
import { Product } from 'types';

interface ProductCardProps extends Product {
    addToCart(id: string): void;
    isInCard: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },

        card: {
            margin: '15px 0',
            padding: '15px 15px',
        },

        avatar: {
            maxWidth: theme.spacing(15),
            maxHeight: theme.spacing(15),
            width: '100%',
            height: 'auto',
        },

        name: {
            margin: '0',
            fontSize: '1.2rem',
            fontWeight: 'bold',
        },

        button: {
            margin: '2px 5px',
        },
    }),
);

export function ProductCard({ addToCart, name, shortDescription, miniature, price, id, isInCard }: ProductCardProps) {
    const styles = useStyles();
    const history = useHistory();

    const handleAddToCart = useCallback(
        (e: MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            addToCart(id);
        },
        [id, addToCart],
    );

    const handleGoToDetails = useCallback(
        (e: MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            history.push(`/product/${id}`);
        },
        [id, history],
    );

    return (
        <Card className={styles.card}>
            <Grid container spacing={2}>
                <Grid item>
                    <Avatar alt={shortDescription} src={miniature} variant="rounded" className={styles.avatar}>
                        <HelpIcon className={styles.avatar} />
                    </Avatar>
                </Grid>
                <Grid item xs={12} sm container>
                    <Grid item xs container direction="column">
                        <Grid justify="space-between" item container>
                            <Grid item>
                                <p className={styles.name}>{name}</p>
                            </Grid>
                            <Grid item>
                                <Chip variant="outlined" color="primary" label={price} icon={<AttachMoneyIcon />} />
                            </Grid>
                        </Grid>
                        <Grid item>
                            <p>{shortDescription}</p>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container justify="center">
                    <Grid item>
                        <Button
                            className={styles.button}
                            onClick={handleAddToCart}
                            variant="contained"
                            color="primary"
                            startIcon={<AddShoppingCartIcon />}
                            disabled={isInCard}
                        >
                            Add to cart
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            className={styles.button}
                            onClick={handleGoToDetails}
                            variant="contained"
                            color="primary"
                            endIcon={<ArrowForwardIcon />}
                        >
                            Go to details
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
}
