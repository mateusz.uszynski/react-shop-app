import React, { useState, useEffect, useCallback, ChangeEvent, useContext } from 'react';
import { getProducts, CartContext, CartAction, CartActionType } from 'services';
import {
    CircularProgress,
    makeStyles,
    Theme,
    createStyles,
    FormControl,
    Select,
    MenuItem,
    InputLabel,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { ProductCard } from './components';
import { useSnackbar } from 'notistack';
import { Product, ProductId } from 'types';
import { isInCart } from 'utils';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        subHeader: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        elementOnPage: {
            display: 'flex',
            alignItems: 'center',
        },
        formControl: {
            margin: theme.spacing(2),
            minWidth: 150,
        },
        select: {
            marginTop: theme.spacing(1),
        },
        label: {
            marginTop: theme.spacing(1),
        },
        elementOnPageSelect: {
            marginLeft: theme.spacing(2),
        },
        pagination: {
            display: 'flex',
            justifyContent: 'center',
            margin: theme.spacing(3),
        },
    }),
);

export function ProductPage() {
    const { cart, dispatchCart } = useContext(CartContext);
    const [products, setProducts] = useState<Product[] | null>(null);
    const [totalDataCount, setTotalDataCount] = useState<number>(0);
    const [elementOnPage, setElementOnPage] = useState<number>(5);
    const [page, setPage] = useState<number>(1);
    const styles = useStyles();
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        setTimeout(() => {
            getProducts(page, elementOnPage).then((resp) => {
                setProducts(resp.data);
                setTotalDataCount(resp.totalDataCount);
            });
        }, 500);
    }, [page, elementOnPage]);

    const addToCart = useCallback(
        (productId: ProductId) => {
            dispatchCart({ type: CartActionType.ADD_TO_CART, productId } as CartAction);
            enqueueSnackbar('The product has been added to the cart', { variant: 'success' });
        },
        [dispatchCart, enqueueSnackbar],
    );

    const handleChangePage = useCallback(
        (e: unknown, newPage: number) => {
            if (newPage !== page) {
                setProducts(null);
                setPage(newPage);
            }
        },
        [page],
    );

    const changeElementsOnPage = useCallback((e: React.ChangeEvent<{ value: unknown }>) => {
        setElementOnPage(parseInt(e.target.value as string));
        setPage(1);
    }, []);

    return (
        <div>
            <div className={styles.subHeader}>
                <h2>Products</h2>
                <div className={styles.elementOnPage}>
                    <FormControl variant="filled" className={styles.formControl}>
                        <InputLabel id="element-on-page-label" className={styles.label}>
                            Element on page
                        </InputLabel>
                        <Select
                            labelId="element-on-page-label"
                            id="element-on-page-select"
                            className={styles.select}
                            value={elementOnPage}
                            onChange={changeElementsOnPage}
                        >
                            <MenuItem value={5}>5</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={15}>15</MenuItem>
                            <MenuItem value={20}>20</MenuItem>
                        </Select>
                    </FormControl>
                </div>
            </div>
            {products === null && <CircularProgress />}
            {products !== null && products.length === 0 && <p>List is empty!</p>}
            {products !== null && products.length > 0 && (
                <>
                    {products.map((product: Product) => (
                        <ProductCard
                            {...product}
                            addToCart={addToCart}
                            key={product.id}
                            isInCard={isInCart(product.id, cart)}
                        />
                    ))}
                    <Pagination
                        className={styles.pagination}
                        page={page}
                        count={Math.ceil(totalDataCount / elementOnPage)}
                        color="primary"
                        onChange={handleChangePage}
                    />
                </>
            )}
        </div>
    );
}
