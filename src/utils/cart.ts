import { ProductId, CartItem, Product } from 'types';
import { find } from 'lodash';

export function isInCart(id: ProductId, cart: CartItem[]): boolean {
    return find(cart, { id }) ? true : false;
}

export function getTotalPrice(products: Product[], cart: CartItem[]): number {
    let totalPrice = 0;
    cart.forEach((item: CartItem) => {
        totalPrice += (find(products, { id: item.id })?.price ?? 0) * item.count;
    });
    return +totalPrice.toFixed(2);
}
