import { AddressValidationMessages, AddressValidationProps } from 'types';

export const validateAddressForm = (values: AddressValidationProps) => {
    const errors: AddressValidationMessages = {};
    if (!values.firstName) errors.firstName = 'Required';
    else if (values.firstName.length > 32) errors.firstName = 'Too long';
    if (!values.lastName) errors.lastName = 'Required';
    else if (values.lastName.length > 32) errors.lastName = 'Too long';
    if (!values.street) errors.street = 'Required';
    else if (values.street.length > 32) errors.street = 'Too long';
    if (!values.buildingNo) errors.buildingNo = 'Required';
    else if (!/^\d+$/.test(values.buildingNo.toString())) errors.buildingNo = 'Must be a number';
    if (!values.postalCode) errors.postalCode = 'Required';
    else if (!/^[0-9]{2}-[0-9]{3}$/.test(values.postalCode)) errors.postalCode = 'Invalid format';
    if (!values.city) errors.city = 'Required';
    else if (values.city.length > 32) errors.city = 'Too long';
    if (!values.country) errors.country = 'Required';
    else if (values.country.length > 32) errors.country = 'Too long';
    return errors;
};
