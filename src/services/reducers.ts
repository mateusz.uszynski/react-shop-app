import { CartItem, ProductId } from 'types';
import { cond, matches, find } from 'lodash';

export type CartAction = { type: CartActionType; productId: ProductId } & {
    type: CartActionType;
    productId: ProductId;
    count: number;
};

export enum CartActionType {
    ADD_TO_CART = 'ADD_TO_CART',
    REMOVE_FROM_CART = 'REMOVE_FROM_CART',
    INCREMENT = 'INCREMENT',
    DECREMENT = 'DECREMENT',
    CHANGE_AMOUNT = 'CHANGE_AMOUNT',
    CLEAR_CART = 'CLEAR_CART',
}

export function cartReducer(state: CartItem[] = [], action: CartAction): CartItem[] {
    return cond([
        [
            matches({ type: CartActionType.ADD_TO_CART }),
            () => {
                const inArray = find(state, { id: action.productId });
                return inArray ? state : [...state, { id: action.productId, count: 1 }];
            },
        ],
        [matches({ type: CartActionType.REMOVE_FROM_CART }), () => state.filter(({ id }) => id !== action.productId)],
        [
            matches({ type: CartActionType.INCREMENT }),
            () => state.map(({ id, count }) => ({ id, count: id === action.productId ? count + 1 : count })),
        ],
        [
            matches({ type: CartActionType.DECREMENT }),
            () =>
                state.map(({ id, count }) => ({ id, count: Math.max(id === action.productId ? count - 1 : count, 1) })),
        ],
        [
            matches({ type: CartActionType.CHANGE_AMOUNT }),
            () =>
                state.map((product) =>
                    product.id === action.productId ? { ...product, count: action.count } : product,
                ),
        ],
        [matches({ type: CartActionType.CLEAR_CART }), () => []],
        [() => true, () => state],
    ])(action);
}
