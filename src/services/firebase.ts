import firebase from 'firebase';
import { Product, ProductId } from 'types';
import { Order } from 'types/Order';

const firebaseConfig = {
    apiKey: 'AIzaSyBfcwPGPczbjv5zSg09rE9023w-q4txqHY',
    authDomain: 'react-shop-8a252.firebaseapp.com',
    databaseURL: 'https://react-shop-8a252.firebaseio.com',
    projectId: 'react-shop-8a252',
    storageBucket: 'react-shop-8a252.appspot.com',
    messagingSenderId: '980439180533',
    appId: '1:980439180533:web:8f2e766d5e820d727a91bc',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAuth = firebaseApp.auth();

export const firebaseService = {
    getProducts(page: number, elementsOnPage: number) {
        return firebaseApp
            .database()
            .ref('/products')
            .once('value')
            .then((resp) => {
                return {
                    data: Object.values(resp.val().data).slice(
                        (page - 1) * elementsOnPage,
                        page * elementsOnPage,
                    ) as Product[],
                    totalDataCount: resp.val().totalDataCount as number,
                };
            });
    },
    getAllProducts() {
        return firebaseApp
            .database()
            .ref('/products')
            .once('value')
            .then((resp) => {
                return {
                    data: Object.values(resp.val().data) as Product[],
                    totalDataCount: resp.val().totalDataCount as number,
                };
            });
    },
    getProduct(productId: ProductId) {
        return firebaseApp
            .database()
            .ref(`/products/data/${productId}`)
            .once('value')
            .then((resp) => resp.val() as Product);
    },
    addOrder(order: Order) {
        return firebaseApp.database().ref('/orders').push(order);
    },
    async signInWithGoogle() {
        await firebaseAuth.useDeviceLanguage();
        await firebaseAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        return await firebaseAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    },
    getRedirectResult() {
        return firebaseAuth.getRedirectResult();
    },
    signOut() {
        return firebaseAuth.signOut();
    },

    onAuthStateChanged: firebaseAuth.onAuthStateChanged.bind(firebaseApp.auth()),
};
