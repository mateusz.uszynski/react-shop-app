import { firebaseService } from 'services';
import { AddressForm, CartItem } from 'types';

export function addOrder(address: AddressForm, cart: CartItem[], user: firebase.User): void {
    firebaseService.addOrder({
        address,
        cart,
        user: {
            displayName: user.displayName,
            email: user.email,
            phoneNumber: user.phoneNumber,
            providerId: user.providerId,
            uid: user.uid,
        } as firebase.UserInfo,
    });
}
