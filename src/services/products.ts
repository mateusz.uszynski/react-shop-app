import { firebaseService } from 'services';
import { Product, ProductId } from 'types';

export function getProduct(productId: ProductId): Promise<Product> {
    return firebaseService.getProduct(productId);
}

export function getProducts(page: number, elementsOnPage = 10): Promise<{ data: Product[]; totalDataCount: number }> {
    return firebaseService.getProducts(page, elementsOnPage);
}

export function getAllProducts(): Promise<{ data: Product[]; totalDataCount: number }> {
    return firebaseService.getAllProducts();
}
