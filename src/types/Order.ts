import { CartItem } from './CartItem';
import { AddressForm } from './AddressForm';

export type Order = {
    address: AddressForm;
    cart: CartItem[];
    user: firebase.UserInfo;
};
