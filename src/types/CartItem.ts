import { ProductId } from 'types';

export type CartItem = {
    id: ProductId;
    count: number;
};
