export type Field = {
    size: boolean | 6 | 'auto' | 1 | 2 | 3 | 4 | 5 | 7 | 8 | 9 | 10 | 11 | 12 | undefined;
    field: JSX.Element;
};

export interface AddressForm {
    readonly title: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly street: string;
    readonly buildingNo: number;
    readonly apartmentNo?: number;
    readonly city: string;
    readonly postalCode: string;
    readonly country: string;
}

export type AddressValidationProps = {
    title?: string;
    firstName?: string;
    lastName?: string;
    street?: string;
    buildingNo?: number;
    apartmentNo?: number;
    city?: string;
    postalCode?: string;
    country?: string;
};

export type AddressValidationMessages = {
    title?: string;
    firstName?: string;
    lastName?: string;
    street?: string;
    buildingNo?: string;
    apartmentNo?: string;
    city?: string;
    postalCode?: string;
    country?: string;
};
